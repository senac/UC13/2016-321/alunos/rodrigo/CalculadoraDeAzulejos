/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.calculadoradeazulejos;

/**
 *
 * @author Diamond
 */
public class ComodoRegular extends Comodo{

    private double comprimento;
    private double largura;
    private double altura;

    public ComodoRegular() {
    }

    public ComodoRegular(double comprimento, double largura, double altura) {
        this.comprimento = comprimento;
        this.largura = largura;
        this.altura = altura;
    }

    public double getComprimento() {
        return comprimento;
    }

    public void setComprimento(double comprimento) {
        this.comprimento = comprimento;
    }

    public double getLargura() {
        return largura;
    }

    public void setLargura(double largura) {
        this.largura = largura;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public double getAreaTotal() {
        return (2 * (this.comprimento * this.altura)) + (2 * (this.largura * this.altura));
    }

}
