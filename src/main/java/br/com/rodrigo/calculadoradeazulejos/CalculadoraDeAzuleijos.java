/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.calculadoradeazulejos;

/**
 *
 * @author Diamond
 */
public class CalculadoraDeAzuleijos {
    
      private double AZULEIJO = 1.5;

    public int calcular(Comodo comodo) {

        double area = comodo.getAreaTotal();
        int caixas = (int) Math.ceil(area / AZULEIJO);
        return caixas;
    }
    
}
