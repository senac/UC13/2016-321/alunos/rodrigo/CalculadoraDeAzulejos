package br.com.rodrigo.calculadoradeazulejosTest;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Diamond
 */
import br.com.rodrigo.calculadoradeazulejos.ComodoRegular;
import static org.junit.Assert.*;
import org.junit.Test;

public class CozinhaTest {

    @Test
    public void deveCalcularAreaTotalCozinha() {
        ComodoRegular cozinha = new ComodoRegular(1, 0.5, 0.5);
        double area = cozinha.getAreaTotal();
        assertEquals(1.5, area , 0);

    }

}